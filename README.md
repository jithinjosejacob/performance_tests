# performance_tests

Performance tests using k6

Install K6 Windows - https://dl.bintray.com/loadimpact/windows/k6-latest-amd64.msi

Run tests- k6 run -u 10 -d 30s .\performance_tests\perf_tests.js

Run Tests with Visualisation - k6 run -u 10 -d 30s --out influxdb=http://localhost:8086/mydb .\performance_tests\perf_tests.js

**Gitlab connection from local - **

git remote add origin <url of repo>